import React from "react";

const Item = () => {
  return (
    <div className="container ">
      {/* <div className="row mt-3 ">
        <div className="card col-3">
          <img
            className="img-fluid"
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card col-3">
          <img
            className="img-fluid"
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            alt="..."
          />

          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card col-3">
          <img
            className="img-fluid"
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            alt="..."
          />

          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card col-3">
          <img
            className="img-fluid"
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            alt="..."
          />

          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div> */}
      <div className="card-deck" style={{ margin: -30 }}>
        <div className="card ">
          <img
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              This is a wider card with supporting text below as a natural
              lead-in to additional content. This content is a little bit
              longer.
            </p>
          </div>
          <div className="card-footer">
            <button className="btn btn-primary">Find Out More</button>
          </div>
        </div>
        <div className="card">
          <img
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              This is a wider card with supporting text below as a natural
              lead-in to additional content. This content is a little bit
              longer.
            </p>
          </div>
          <div className="card-footer">
            <button className="btn btn-primary">Find Out More</button>
          </div>
        </div>
        <div className="card">
          <img
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              This card has supporting text below as a natural lead-in to
              additional content.
            </p>
          </div>
          <div className="card-footer">
            <button className="btn btn-primary">Find Out More</button>
          </div>
        </div>
        <div className="card ">
          <img
            src="./Ice-Cream-Scoop-PNG-Picture.png"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              This is a wider card with supporting text below as a natural
              lead-in to additional content. This card has even longer content
              than the first to 
            </p>
          </div>
          <div className="card-footer">
            <button className="btn btn-primary">Find Out More</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Item;
