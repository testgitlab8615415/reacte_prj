import React from "react";

const Bannerr = () => {
  return (
    <div className="p-5">
      <div className="card text-center container p-5 ">
        <div className="card-body text-left">
          <h1 className="card-title font-weight-light" style={{ fontSize: 60 }}>
            A warm welcome!
          </h1>
          <p className="card-text ">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facere
            omnis, magnam esse cupiditate corporis iste tenetur. Animi, quo
            corporis rem omnis, delectus cumque tenetur iste molestiae explicabo
            beatae ipsam praesentium?
          </p>
          <button className="btn btn-primary">
            Call to action
          </button>
            
          
        </div>
      </div>
    </div>
  );
};

export default Bannerr;
