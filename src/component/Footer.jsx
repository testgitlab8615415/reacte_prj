import React from 'react'

const Footer = () => {
  return (
    <div className='bg-dark mt-5'>
      <p className='p-5 text-white' >Copyright © Your Website 2023</p>
    </div>
  )
}

export default Footer