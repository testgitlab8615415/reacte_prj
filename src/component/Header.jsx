import React from "react";

const Header = () => {
  return (
    <div className=" bg-dark">
      <nav className="navbar navbar-expand-lg navbar-dark container">
        <a className="navbar-brand" href="#">
          Start Bootstrap
        </a>
        <div className="collapse navbar-collapse" >
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <a className="nav-link" href="#">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Service
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </nav>
      
    </div>
  );
};

export default Header;
